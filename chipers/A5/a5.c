#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

char *input, *output;
int fd_inp, fd_out;
uint32_t r1, r2, r3;

int open_file_r(char *file_name) {
	int fd = open(file_name, O_RDONLY);
	if (fd < 0) {
		printf("cant open file %s\n", file_name);
		exit(1);
	}
	return fd;
}

int open_file_w(char *file_name) {
	int fd = open(file_name, O_WRONLY| O_TRUNC | O_CREAT, 0666);
	if (fd < 0) {
		printf("cant open file %s\n", file_name);
		exit(1);
	}
	return fd;
}

// arguments are in order:
// input, output
void parse_request(int argc, char **argv) {
	if (argc != 3) {
		printf("wrong count of parameters\n");
		exit(1);
	}
	fd_inp = open_file_r(argv[1]);
	fd_out = open_file_w(argv[2]);
}

void init_registers() {
	r1 = 0x1111f;
	r2 = 0x123123;
	r3 = 0x2ff2ff;
}

void encrypt_a5() {
	uint8_t cur;
	uint8_t res;
	uint8_t right_bit;
	while (read(fd_inp, &cur, 1)) {
		res = 0x00;
		for (int i = 0; i < 8; i++) {
			res <<= 1;
			res |= (((r1 & (1 << 18)) >> 18) ^ ((r2 & (1 << 21)) >> 21) ^ ((r3 & (1 << 22)) >> 22));
			right_bit = ((r1 & (1 << 13)) >> 13) ^ ((r1 & (1 << 16)) >> 16) ^ ((r1 & (1 << 17)) >> 17) ^ ((r1 & (1 << 18)) >> 18);
			r1 <<= 1;
			r1 |= right_bit;
			right_bit = ((r2 & (1 << 21)) >> 21) ^ ((r2 & (1 << 20)) >> 20);
			r2 <<= 1;
			r2 |= right_bit;
			right_bit = ((r3 & (1 << 22)) >> 22) ^ ((r3 & (1 << 21)) >> 21) ^ ((r3 & (1 << 20)) >> 20);
			r3 <<= 1;
			r3 |= right_bit;
		}
		res ^= cur;
		write(fd_out, &res, 1);
	}
}

void close_files() {
	close(fd_inp);
	close(fd_out);
}

int main(int argc, char **argv) {
	parse_request(argc, argv);
	init_registers();
	encrypt_a5();
	close_files();
	return 0;
}