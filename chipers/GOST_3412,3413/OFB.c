#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

int e_opt;
char *input, *output, *key_file, *iv_file;
int fd_inp, fd_out, fd_key, fd_iv;
uint64_t block, chiper_block;
char c; // buffer for testing that file is ended
uint32_t primary_key[8];
uint32_t keys[32];
const unsigned char pi[8][16] = {
	{12, 4, 6, 2, 10, 5, 11, 9, 14, 8, 13, 7, 0, 3, 15, 1},
	{6, 8, 2, 3, 9, 10, 5, 12, 1, 14, 4, 7, 11, 13, 0, 15},
	{11, 3, 5, 8, 2, 15, 10, 13, 14, 1, 7, 4, 12, 9, 6, 0},
	{12, 8, 2, 1, 13, 4, 15, 6, 7, 0, 10, 5, 3, 14, 9, 11},
	{7, 15, 5, 10, 8, 1, 6, 13, 0, 9, 3, 14, 11, 4, 2, 12},
	{5, 13, 15, 6, 9, 2, 12, 10, 11, 7, 8, 1, 4, 3, 14, 0},
	{8, 14, 2, 5, 6, 9, 1, 12, 15, 4, 11, 0, 13, 10, 3, 7},
	{1, 7, 14, 13, 0, 5, 8, 3, 4, 15, 10, 6, 9, 12, 11, 2},
};

int open_file_r(char *file_name) {
	int fd = open(file_name, O_RDONLY);
	if (fd < 0) {
		printf("cant open file %s\n", file_name);
		exit(1);
	}
	return fd;
}

int open_file_w(char *file_name) {
	int fd = open(file_name, O_WRONLY| O_TRUNC | O_CREAT, 0666);
	if (fd < 0) {
		printf("cant open file %s\n", file_name);
		exit(1);
	}
	return fd;
}

// arguments are in order:
// input, output, key, IV, encrypt/decrypt
void parse_request(int argc, char **argv) {
	if (argc != 6) {
		printf("wrong count of parameters\n");
		exit(1);
	}
	fd_inp = open_file_r(argv[1]);
	fd_out = open_file_w(argv[2]);
	fd_key = open_file_r(argv[3]);
	fd_iv = open_file_r(argv[4]);
	if (!strcmp(argv[5], "-e")) {
		e_opt = 1;
	} else if (!strcmp(argv[5], "-d")) {
		e_opt = 0;
	} else {
		printf("last paramater must be -e or -d\n");
		exit(1);
	}
}

// Litle Endian -> Big Endian
uint32_t reverse_int32(const uint32_t a) {
	uint32_t res = 0, temp;
	uint32_t mask = 0xFF000000; 
	for (int i = 0; i < 4; i++) {
		res = res >> 8;
		temp = a & mask;
		temp = temp << (i * 8);
		res = res | temp;
		mask = mask >> 8;
	}
	return res;
}

// Litle Endian -> Big Endian
uint64_t reverse_int64(const uint64_t a) {
	uint64_t res = 0, temp;
	uint64_t mask = (uint64_t)0xFF << (8 * 7); 
	for (int i = 0; i < 8; i++) {
		res = res >> 8;
		temp = a & mask;
		temp = temp << (i * 8);
		res = res | temp;
		mask = mask >> 8;
	}
	return res;
}

void key_schedule() {
	if (read(fd_key, &primary_key, 32) < 32) {
		printf("key must have 256 bit length\n");
		exit(0);
	}
	if (read(fd_key, &c, 1) != 0) {
		printf("key must have 256 bit length\n");
		exit(0);
	}
	for (int i = 0; i < 8; i++) {
		primary_key[i] = reverse_int32(primary_key[i]);
	}
	for (int i = 0; i < 8; i++) {
		keys[i] = primary_key[i];
	}
	for (int i = 0; i < 8; i++) {
		keys[i + 8] = keys[i + 16] = keys[i];
		keys[i + 24] = keys[7 - i];
	}
	return;
}

uint32_t t_proc(const uint32_t a) {
	unsigned char left, right;
	uint32_t res, mask, temp;
	mask = 0xFF;
	res = 0x00;
	for (int i = 0; i < 4; i++) {
		left = right = (a & mask) >> (8 * i);
		left = left >> 4;
		right = right & 0x0F;
		left = pi[2 * i + 1][left];
		right = pi[2 * i][right];
		temp = (left << 4) | right;
		temp = temp << (8 * i);
		res = res | temp;
		mask = mask << 8;
	}
	return res;
}

uint32_t g_proc(const uint32_t k, const uint32_t a) {
	uint32_t b, c;
	b = c = t_proc(a + k);
	c = c >> (32 - 11);
	b = b << 11;
	return b | c;
}

void G_proc(const uint32_t left, const uint32_t right, const uint32_t key, uint32_t *left_new, uint32_t *right_new) {
	uint32_t save = right;
	*right_new = g_proc(right, key) ^ left;
	*left_new = right;
	return;
}

int64_t G_last_proc(const uint32_t left, const uint32_t right, const uint32_t key) {
	int64_t res = (g_proc(right, key) ^ left);
	res = res << 32;
	return res | right;
}

int64_t basic_encrypt(const int64_t block) {
	uint32_t left, right, left_new, right_new;
	left = block >> 32;
	right = block << 32 >> 32;
	for (int i = 0; i < 31; i++) {
		G_proc(left, right, keys[i], &left_new, &right_new);
		left = left_new;
		right = right_new;
	}
	return G_last_proc(left, right, keys[31]);
}

int64_t basic_decrypt(const int64_t block) {
	uint32_t left, right, left_new, right_new;
	left = block >> 32;
	right = block << 32 >> 32;
	for (int i = 31; i > 0; i--) {
		G_proc(left, right, keys[i], &left_new, &right_new);
		left = left_new;
		right = right_new;
	}
	return G_last_proc(left, right, keys[0]);
}

//s = 64; m = 128;
void OFB() {
	int n;
	uint64_t IV[2], temp;
	
	if (fd_iv != 0) {
		if (read(fd_iv, &IV, 16) < 16) {
			printf("IV must have 128 bit length\n");
			exit(0);
		}
		if (read(fd_iv, &c, 1) != 0) {
			printf("IV must have 128 bit length\n");
			exit(0);
		}
		for (int i = 0; i < 2; i++) {
			IV[i] = reverse_int64(IV[i]);
		}
	} else {
		for (int i = 0; i < 2; i++) {
			IV[i] = 0x00;	
		}
	}

	if (e_opt) {
		while ((n = read(fd_inp, &block, 8)) == 8) {
			block = reverse_int64(block);
			temp = chiper_block = basic_encrypt(IV[0]);
			chiper_block = chiper_block ^ block;
			IV[0] = IV[1];
			IV[1] = temp;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, &chiper_block, 8);
		}
		if (n != 0) {
			chiper_block = basic_encrypt(IV[0]);
			block = reverse_int64(block);
			chiper_block = chiper_block ^ block;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, (unsigned char *)&chiper_block + 8 - n, n);
		}
	} else {
		unsigned long ct = 0, i = 0;
		while ((n = read(fd_inp, &block, 8)) == 8) {
			ct++;
		}
		lseek(fd_inp, 0, SEEK_SET);
		if (n != 0) {
			printf("chiper text must have len 64 * n\n");
			exit(0);
		}
		while ((n = read(fd_inp, &block, 8)) == 8) {
			block = reverse_int64(block);
			temp = chiper_block = basic_encrypt(IV[0]);
			chiper_block = chiper_block ^ block;
			IV[0] = IV[1];
			IV[1] = temp;
			chiper_block = reverse_int64(chiper_block);
			write(fd_out, &chiper_block, 8);
		}
	}
}

void close_files() {
	close(fd_inp);
	close(fd_key);
	close(fd_out);
	close(fd_iv);
}

int main(int argc, char **argv) {
	parse_request(argc, argv);
	key_schedule();
	OFB();
	close_files();
	return 0;
}