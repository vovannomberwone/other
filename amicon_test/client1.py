from socket import *
import os
import sys
import struct

def close_socs(*socs):
	for soc in socs:
		soc.close()

def tcp_send(my_addr, reciver_addr, file):
	tcp_socket = socket(AF_INET, SOCK_STREAM)
	if not tcp_socket:
		print('Cant open socket')
		sys.exit()
	tcp_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	tcp_socket.bind(my_addr)
	tcp_socket.listen(1)
	while True:
		conn, addr_conn = tcp_socket.accept()
		if addr_conn[0] == reciver_addr[0]:
			break
	f = open(file, "rb")
	while True:
		data = f.read(1024)
		if not data:
			conn.send(struct.pack('I', 0))
			break
		conn.send(struct.pack('I', len(data)))
		conn.send(data)
	close_socs(conn, tcp_socket)
	f.close()

def udp_recieve(my_addr, sender_addr, file):
	udp_socket = socket(AF_INET, SOCK_DGRAM)
	udp_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	udp_socket.bind(my_addr)
	if not udp_socket:
		print('Cant open socket')
		sys.exit()
	f = open(file, "wb")
	while True:
		while True:
			length, addr = udp_socket.recvfrom(4)
			if addr[0] == sender_addr[0]:
				break
		length = struct.unpack('I', length)[0]
		if length == 0:
			break
		while True:
			data, addr = udp_socket.recvfrom(length)
			if addr[0] == sender_addr[0]:
				break
		f.write(data)
	udp_socket.close()
	f.close()

def main():
	client1 = ('127.0.0.1', 10000)
	client2 = ('127.0.0.1', 10001)
	tcp_send(client1, client2, 'file.txt')
	udp_recieve(client1, client2, 'recv.txt')
	if not(os.system('diff file.txt recv.txt')): 
		print('files are the same')

if __name__ == '__main__':
	main()