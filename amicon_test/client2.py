from socket import *
import struct
import sys
import os

def tcp_recieve(sender_addr, file):
	tcp_socket = socket(AF_INET, SOCK_STREAM)
	tcp_socket.connect(sender_addr)
	f = open(file, 'wb')
	while True:
		length = tcp_socket.recv(4)
		length = struct.unpack('I', length)[0]
		if length == 0:
			break
		data = tcp_socket.recv(length)
		f.write(data)
	tcp_socket.close()

def udp_send(my_addr, receiver_addr, file):
	udp_socket = socket(AF_INET, SOCK_DGRAM)
	if not udp_socket:
		print('Cant open socket')
		os.system('rm ' + file)
		sys.exit()
	udp_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	udp_socket.bind(my_addr)
	f = open(file, "rb")
	while True:
		data = f.read(1024)
		if not data:
			udp_socket.sendto(struct.pack('I', 0), receiver_addr)
			break
		udp_socket.sendto(struct.pack('I', len(data)), receiver_addr)
		udp_socket.sendto(data, receiver_addr)
	udp_socket.close()
	f.close()
	os.system('rm tempfile')

def main():
	client1 = ('127.0.0.1', 10000)
	client2 = ('127.0.0.1', 10001)
	tcp_recieve(client1, 'tempfile')
	udp_send(client2, client1, 'tempfile')
	
if __name__ == '__main__':
	main()