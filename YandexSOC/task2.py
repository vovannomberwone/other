import os
import re
import json
import requests

def validate_email(email):
	return re.search('\.info$', email)

def main():
	response = requests.get('https://jsonplaceholder.typicode.com/comments')
	comments = json.loads(response.text)
	out = open('res.csv', 'w')
	out.write('email,words_count\n')

	for com in comments:
		wc = len(com['name'].split())
		email = com['email']
		if (wc < 4 and validate_email(email)):
			out.write(email + ',' + str(wc) + '\n')

	out.close()

if __name__ == '__main__':
	main()