import os
import re

def validate_login(login):
	return re.fullmatch('^[a-z][a-z0-9-]{1,13}[a-z0-9]$', login)  

def main():
	inp = open('file.csv', 'r')
	out = open('res.csv', 'w')
	head = ''.join(inp.readline().split('\n')[0].split()).split(',')
	
	while (1):
		print('Enter 2 columns names and N - position login column')
		col1 = input()
		col2 = input()
		N = int(input())
		if (not col1 == '' and not col2 == '' and not(col1 == col2) \
							and (N <= len(head))):
			i = 0
			pos1 = -1
			pos2 = -1
			for col_name in head:
				if (col1 == col_name):
					pos1 = i
				if (col2 == col_name):
					pos2 = i
				i += 1
			if (pos1 >= 0 and pos2 >= 0):
				break

	out.write(col1 + ',' + col2 + '\n')
	for line in inp:
		l = ''.join(line.split('\n')[0].split()).split(',')
		login = l[N - 1]
		col1 = l[pos1]
		col2 = l[pos2]
		if (validate_login(login)):
			out.write(col1 + ',' + col2 + '\n')

	inp.close()
	out.close()

if __name__ == '__main__':
	main()